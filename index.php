<?php

/*** error reporting on ***/
error_reporting(E_ALL);

/*** define the site path ***/
$site_path = realpath(dirname(__FILE__));
define('__SITE_PATH', $site_path);
define('__SITE_URL', "http://" . $_SERVER['SERVER_NAME'] . "/prax4/");
define('PSW', "password");
define('PSW_L', "password_login");
define('USR', "username");
define('USR_L', "username_login");
define('GER', "global_form_error");
define('EML', "email");
define('GEN', "gender");
define('DSC', "description");
define('RAD', "radius");
define('FN', "full_name");

/*** include the init.php file ***/
include 'includes/init.php';

/*** load the router ***/
$registry->router = new router($registry);

/*** set the controller path ***/
$registry->router->setPath(__SITE_PATH . '/controller');

/*** load up the template ***/
$registry->template = new template($registry);

/*** load the controller ***/
$registry->router->loader();

function get_content($URL){
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_URL, $URL);
    $data = curl_exec($ch);
    curl_close($ch);
    return $data;
}

?>
