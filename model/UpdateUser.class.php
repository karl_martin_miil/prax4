<?php

/**
 * Created by PhpStorm.
 * User: karl
 * Date: 3.12.2015
 * Time: 10:32
 */
class UpdateUser
{

    private $db;

    public function __construct($db)
    {
        $this->db = $db;
    }

    public function updateUserInfoInDb($userInfo)
    {
        $sql = "UPDATE person SET `gender` = ?, `fullname` = ?, `description` = ?, `radius` = ? WHERE username = ?;";
        $stmt = $this->db->prepare($sql);
        //exit(var_dump($userInfo));
        return $stmt->execute($userInfo);

    }

    public function makeUserAccountInfoArray()
    {
        $username = $_SESSION['username'];
        $gender = isset($_POST['gender']) ? $_POST['gender'] : "";
        $radius = isset($_POST['radius']) ? $_POST['radius'] : "";
        $fullName = isset($_POST['fullname']) ? $_POST['fullname'] : "";
        $description = isset($_POST['description']) ? $_POST['description'] : "";
        return array($gender, $fullName, $description, $radius, $username);
    }
}