<?php

/**
 * Created by PhpStorm.
 * User: karl
 * Date: 3.12.2015
 * Time: 10:36
 */
class UserDbInfoParser
{

    private $db;

    public function __construct($db)
    {
        $this->db = $db;
    }

    public function checkIfAllFieldsAreFilled($username)
    {
        $stmt = $this->db->prepare("SELECT * FROM person WHERE username = :username");
        $stmt->bindParam(':username', $username);
        if ($stmt->execute()) {
            $answers = (array)$stmt->fetch();
            return $this->checkAllFields($answers, $username);
        }
        return false;
    }

    private function checkAllFields($answers, $username)
    {
        if (empty($answers['fullname']) || empty($answers['gender']) || empty($answers['description'])) {
            return false;
        } else {
            if ($this->checkIfPictureIsSet($username)) {
                return true;
            }
        }
        return false;
    }

    public function checkIfPictureIsSet($username)
    {
        $stmt = $this->db->prepare("SELECT person_img.url FROM person, person_img
                                    WHERE person_img.person_id = person.id AND username = :username");
        $stmt->bindParam(':username', $username);
        if ($stmt->execute()) {
            if ($stmt->rowCount() > 0) {
                return true;
            }
        }
        return false;
    }


}