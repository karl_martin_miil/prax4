<?php

/**
 * Created by PhpStorm.
 * User: karl
 * Date: 2.12.2015
 * Time: 21:45
 */
class ValidateInput
{

    private $errorArray = array();
    private $db;

    public function __construct($db)
    {
        $this->db = $db;
    }

    public function validateRegister()
    {
        if (isset($_POST["username"]) && isset($_POST["email"])
            && isset($_POST["password"]) && isset($_POST["password_confirmation"])
        ) {
            return $this->parseRegisterInput();
        } else {
            $this->setError(GER, "please fill in all inputs!");
        }
    }

    function parseRegisterInput()
    {
        $username = isset($_POST['username']) ? $_POST['username'] : "";
        $email = isset($_POST['email']) ? $_POST['email'] : "";
        $password = isset($_POST['password']) ? $_POST['password'] : "";
        $passwordConfirmation = isset($_POST['password_confirmation']) ? $_POST['password_confirmation'] : "";

        $this->checkIfVariableEmpty($username, USR);
        $this->checkIfVariableEmpty($email, EML);
        $this->checkIfVariableEmpty($password, PSW);
        $this->checkIfVariableEmpty($passwordConfirmation, PSW);

        $this->validatePassword($password, $passwordConfirmation);
        if (!$this->checkIfUniqueInDb("email", $email)) {
            $this->setError(EML, "email already taken!");
        }
        if (!$this->checkIfUniqueInDb("username", $username)){
            $this->setError(USR, "username already taken!");
        }

        return $this->errorArray;

    }

    function checkIfUniqueInDb($field, $value)
    {
        //exit($field.$value);
        $stmt = $this->db->prepare("SELECT $field FROM person WHERE $field = ?");
        if ($stmt->execute(array($value))) {
            if ($stmt->rowCount() > 0) {
                return false;
            } else {
                return true;
            }
        }
    }

    function checkIfVariableEmpty($variable, $type)
    {
        if ($variable == "") {
            $this->setError($type, "please fill this!");
        }
    }


    function validatePassword($password, $passwordConfirmation)
    {
        if (strlen($password) < 8) {
            $this->setError(PSW, "Password too short!");
        }

        if (!preg_match("#[0-9]+#", $password)) {
            $this->setError(PSW, "Password must include at least one number!");
        }

        if (!preg_match("#[a-zA-Z]+#", $password)) {
            $this->setError(PSW, "Password must include at least one letter!");
        }
        if ($password !== $passwordConfirmation) {
            $this->setError(PSW, "Passwords must match!");
        }
    }

    private function setError($field, $errorMessage)
    {
        $this->errorArray[$field] = $errorMessage;
    }

    public function validateLogin()
    {
        if (isset($_POST["username"]) && isset($_POST["password"])
        ) {
            return $this->parseLoginInput();
        } else {
            $this->setError(GER, "please fill in all inputs!");
        }
        return $this->errorArray;
    }

    private function parseLoginInput()
    {
        $username = isset($_POST['username']) ? $_POST['username'] : "";
        $password = isset($_POST['password']) ? $_POST['password'] : "";
        $this->checkIfVariableEmpty($username, USR);
        $this->checkIfVariableEmpty($password, PSW);
        if ($this->checkIfUniqueInDb("username", $username)){
            $this->setError(USR_L, "no such username");
        } else {
            $this->verifyUserPassword($password, $username);
        }
        return $this->errorArray;
    }

    function verifyUserPassword($password, $username)
    {
        $hashedPassword = $this->getHashedPasswordFromDb($username);
        if(!(crypt($password, $hashedPassword) == $hashedPassword)) {
            $this->setError(PSW_L, "wrong password!");
        }
    }

    private function getHashedPasswordFromDb($username)
    {
        $stmt = $this->db->prepare("SELECT password FROM person WHERE username = :username");
        $stmt->bindParam(':username', $username);
        if ($stmt->execute()) {
            $answers = (array)$stmt->fetch();
            return $answers['password'];
        }
    }

    public function validateUpdate()
    {
        if (isset($_POST["fullname"])
            && isset($_POST["gender"]) && isset($_POST["radius"])
            && isset($_POST["description"]) && isset($_POST["password"])
        ) {
            return $this->parseUpdateInput();
        } else {
            $this->setError(GER, "please fill in all inputs!");
            $this->setError(GEN, "please fill in gender info");
        }
        return $this->errorArray;
    }

    private function parseUpdateInput()
    {

        $password = isset($_POST['password']) ? $_POST['password'] : "";
        $gender = isset($_POST['gender']) ? $_POST['gender'] : "";
        $radius = isset($_POST['radius']) ? $_POST['radius'] : "";
        $fullName = isset($_POST['fullname']) ? $_POST['fullname'] : "";
        $description = isset($_POST['description']) ? $_POST['description'] : "";

        $this->checkIfVariableEmpty($fullName, FN);
        $this->checkIfVariableEmpty($password, PSW);
        $this->checkIfVariableEmpty($gender, GEN);
        $this->checkIfVariableEmpty($radius, RAD);
        $this->checkIfVariableEmpty($description, DSC);
        $this->verifyUserPassword($password, $_SESSION['username']);
        return $this->errorArray;

    }
}