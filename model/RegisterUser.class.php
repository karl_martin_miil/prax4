<?php

/**
 * Created by PhpStorm.
 * User: kmm
 * Date: 01.12.2015
 * Time: 13:10
 */
class RegisterUser
{

    private $db;

    public function __construct($db)
    {
        $this->db = $db;
    }


    function generateHash($password)
    {
        if (defined("CRYPT_BLOWFISH") && CRYPT_BLOWFISH) {
            $salt = '$2y$11$' . substr(md5(uniqid(rand(), true)), 0, 22);
            return crypt($password, $salt);
        }
    }

    function addUserToDatabase($userInfo)
    {
        $sql = "INSERT INTO `person` (`username`, `password`, `email`) VALUES (?, ?, ?);";
        $stmt = $this->db->prepare($sql);
        return $stmt->execute($userInfo);

    }

    function makeUserAccountInfoArray()
    {
        $username = isset($_POST['username']) ? $_POST['username'] : "";
        $email = isset($_POST['email']) ? $_POST['email'] : "";
        $password = isset($_POST['password']) ? $_POST['password'] : "";
        return array($username, $this->generateHash($password), $email);
    }


}