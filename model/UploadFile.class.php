<?php

class UploadFile
{


    private $db;
    private $fileExtension;

    public function __construct($db)
    {
        $this->db = $db;
    }

    function upload_my_file($fileid)
    {
        echo "starting";
        $target_dir = "uploads/";
        $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);

        echo "<p>$target_file " . $target_file;
        $uploadOk = 1;
        $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
        echo "<p>$imageFileType " . $imageFileType;
        $saved_file = $target_dir . $fileid . "." . $imageFileType;
        // Check if image file is a actual image or fake image
        if (isset($_POST["submit"])) {
            $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
            if ($check !== false) {
                echo "File is an image - " . $check["mime"] . ".";
                $uploadOk = 1;
            } else {
                echo "File is not an image.";
                $uploadOk = 0;
            }
        }
        echo "all is fine before checks";
        // Check if file already exists
        if (file_exists($target_file)) {
            echo "Sorry, file already exists.";
            $uploadOk = 0;
        }
        // Check file size
        if ($_FILES["fileToUpload"]["size"] > 5000000) {
            echo "Sorry, your file is too large.";
            $uploadOk = 0;
        }
        // Allow certain file formats
        if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
            && $imageFileType != "gif"
        ) {
            echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
            $uploadOk = 0;
        }
        $this->fileExtension = $imageFileType;
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            echo "Sorry, your file was not uploaded.";
            // if everything is ok, try to upload file
            return false;
        } else {
            if (move_uploaded_file(
                $_FILES["fileToUpload"]["tmp_name"], $saved_file)) {
                echo "<p>The file " . basename($_FILES["fileToUpload"]["name"]) . " has been uploaded.";
                return true;
            } else {
                echo "<p>Sorry, there was an error uploading your file.";
                return false;
            }
        }
    }



    function addPictureUrlToDb($uniId, $personId)
    {
        $url = $uniId.".".$this->fileExtension;
        $sql = "INSERT INTO person_img (`person_id`, `url`) VALUES (?, ?);";
        $stmt = $this->db->prepare($sql);
        return $stmt->execute(array($personId, $url));
    }
}

?>
