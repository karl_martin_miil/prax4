<?php

/**
 * Created by PhpStorm.
 * User: karl
 * Date: 4.12.2015
 * Time: 12:45
 */
class GetPersonToMatch
{

    private $db;

    public function __construct($db)
    {
        $this->db = $db;
    }

    function getPersons($username, $userDbInfoParser)
    {
        $personsWithPicture = array();
        $stmt = $this->db->prepare("SELECT  DISTINCT(person.id), username, fullname, description, latitude, longitude,
             radius, url FROM person JOIN person_img img ON person.id = img.person_id
            WHERE username <> ? AND gender <> (SELECT gender FROM person WHERE username = ?)
            GROUP BY person.id;");
        if ($stmt->execute(array($username, $username))) {
            $answers = $stmt->fetchAll();
            foreach ($answers as $person) {
                if ($userDbInfoParser->checkIfPictureIsSet($person['username'])) {
                    array_push($personsWithPicture, $person);
                }
            }

        }
        return $personsWithPicture;
    }

    function addPersonLike($username, $likeId)
    {
        if (!$this->seeIfPersonAlreadyLiked($username, $likeId)) {
            $sql = "INSERT IGNORE INTO person_like (liker_id, liked_id) VALUES ((SELECT id FROM person WHERE username=?), ?) ;";
            $stmt = $this->db->prepare($sql);
            return $stmt->execute(array($username, $likeId));
        }

    }

    function seeIfPersonAlreadyLiked($username, $likeId)
    {
        $sql = "SELECT id FROM person_like WHERE liker_id = (SELECT id FROM person WHERE username=?) AND liked_id = ?";
        $stmt = $this->db->prepare($sql);
        if ($stmt->execute(array($username, $likeId))) {
            if ($stmt->rowCount() > 0) {
                return true;
            } else {
                return false;
            }
        }
    }

    function getAllMatches($username)
    {
        /*$sql = "SELECT username, A.liker_id AS userA, url, fullname, description FROM person_like A
                  JOIN person_like B ON A.liker_id = B.liked_id
                  JOIN person per ON A.liker_id = per.id
                  JOIN person_img img on A.liker_id = img.person_id
                  AND A.liked_id = B.liker_id AND A.id <> B.id AND A.liker_id <> (SELECT id FROM person WHERE username=?)
                GROUP BY A.liker_id;";*/
        $sql = "SELECT username, A.liked_id AS userA, url, fullname, description FROM person_like A
                  JOIN person_like B ON A.liker_id = B.liked_id
                  JOIN person per ON A.liked_id = per.id
                  JOIN person_img img on A.liker_id = img.person_id
                  AND A.liked_id = B.liker_id AND A.id <> B.id AND A.liker_id = (SELECT id FROM person WHERE username=?)
                GROUP BY A.liked_id;";
        $stmt = $this->db->prepare($sql);
        if ($stmt->execute(array($username))) {
            if ($stmt->rowCount() > 0) {
                return $stmt->fetchAll();
            } else {
                return null;
            }
        }
    }
}