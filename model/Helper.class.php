<?php

/**
 * Created by PhpStorm.
 * User: karl
 * Date: 4.12.2015
 * Time: 1:39
 */
class Helper
{

    private $db;

    public function __construct($db)
    {
        $this->db = $db;
    }

    public function getUserId($username) {
        $stmt = $this->db->prepare("SELECT id FROM person WHERE username = ?");
        if ($stmt->execute(array($username))) {
            if ($stmt->rowCount() < 1) {
                return null;
            } else {
                $answers = (array)$stmt->fetch();
                return $answers['id'];
            }
        }
    }

    public function getAllPicturesOfPerson($username) {
        $userId = $this->getUserId($username);
        $stmt = $this->db->prepare("SELECT url FROM person_img WHERE person_id = ?");
        if ($stmt->execute(array($userId))) {
            if ($stmt->rowCount() < 1) {
                return null;
            } else {
                $answers = (array)$stmt->fetchAll();
                return $answers;
            }
        }
    }
    public function getFirstPictureOfPerson($username) {
        $userId = $this->getUserId($username);
        $stmt = $this->db->prepare("SELECT url FROM person_img WHERE person_id = ?");
        if ($stmt->execute(array($userId))) {
            if ($stmt->rowCount() < 1) {
                return null;
            } else {
                $answers = (array)$stmt->fetchAll();
                return $answers;
            }
        }
    }

}