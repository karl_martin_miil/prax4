<?php

/**
 * Created by PhpStorm.
 * User: karl
 * Date: 4.12.2015
 * Time: 11:34
 */
class LogOut
{
    public function __construct() {
        session_unset($_SESSION["username"]);
        session_destroy();
        header('location: '.__SITE_URL);
    }
}