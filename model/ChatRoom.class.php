<?php

/**
 * Created by PhpStorm.
 * User: karl
 * Date: 4.12.2015
 * Time: 22:20
 */
class ChatRoom
{
    private $db;

    public function __construct($db)
    {
        $this->db = $db;
    }

    function getOldChatMessages($username, $subjectId)
    {
        $sql = "SELECT text, time, per.username AS receiver, ner.username AS sender FROM person_chat
  JOIN person per ON receiver_id = per.id
  JOIN person ner ON sender_id = ner.id
WHERE (sender_id = (SELECT id FROM person WHERE username=?) AND receiver_id = ?)
      OR (receiver_id = (SELECT id FROM person WHERE username=?) AND sender_id = ?) ORDER BY time;";
        $stmt = $this->db->prepare($sql);
        $stmt->execute(array($username, $subjectId, $username, $subjectId));
        return $stmt->fetchAll();
    }

    function getNewChatMessages($username, $subjectId, $timestamp)
    {
        $sql = "SELECT text, time, per.username AS receiver, ner.username AS sender FROM person_chat
  JOIN person per ON receiver_id = per.id
  JOIN person ner ON sender_id = ner.id
WHERE (receiver_id = (SELECT id FROM person WHERE username=?) AND sender_id = ?) AND time > ?;";
        $stmt = $this->db->prepare($sql);
        $stmt->execute(array($username, $subjectId, $timestamp));
        return $stmt->fetchAll();
    }

    function submitMessage($username, $subjectId, $text)
    {
        $sql = "INSERT INTO person_chat (sender_id, receiver_id, text)  VALUES ((SELECT id FROM person WHERE username=?), ?, ?);";
        $stmt = $this->db->prepare($sql);
        return $stmt->execute(array($username, $subjectId, $text));
    }
}