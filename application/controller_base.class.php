<?php

Abstract Class baseController
{

    /*
     * @registry object
     */
    protected $registry;
    private $userDbInfoParser;

    function __construct($registry)
    {
        $this->registry = $registry;
        $this->registry->errors = array();
        if (isset($_SESSION['username'])) {
            $this->registry->template->s_username = $_SESSION['username'];
        }
        $this->userDbInfoParser = new UserDbInfoParser($this->registry->db);
        if (isset($_GET['rt']) && !isset($_GET['action'])){
            if (($_GET['rt'] != 'update' && $_GET['rt'] != 'upload')){
                $this->checkIfUserHasFilledAllInfo();
                $this->checkIfUserHasPictures();
            }
        }

    }

    protected function getUsernameFromSession() {
        if (isset($_SESSION['username'])) {
            return $_SESSION['username'];
        } else {
            return "";
        }
    }

    protected function checkIfUserHasFilledAllInfo() {
        $username = $this->getUsernameFromSession();
        if(!$this->userDbInfoParser->checkIfAllFieldsAreFilled($username)){
            header('location: '.__SITE_URL.'?rt=update');
        }
    }

    protected function checkIfUserHasPictures()
    {
        $username = $this->getUsernameFromSession();
        if(!$this->userDbInfoParser->checkIfPictureIsSet($username)) {
            header('location: '.__SITE_URL.'?rt=upload');
        }
    }

    /**
     * @all controllers must contain an index method
     */
    abstract function index();



}

?>
