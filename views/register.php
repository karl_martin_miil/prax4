<?php include(__SITE_PATH . "/views/includes/header.php"); ?>


<div class="container front-page-container full">
    <div class="row centered-form center-vertically">
        <div class="col-xs-12 col-sm-6 col-md-4  col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Please sign up for MiniTinder
                        <small>It's free!</small>
                    </h3>
                </div>
                <div class="panel-body">
                    <form role="form" action="<?php echo __SITE_URL; ?>?action=register" method="post">
                        <?php
                        if (isset($errorArray[GER])) { ?>
                            <div class="alert alert-danger">
                                <?php echo $errorArray[GER];?>
                            </div>
                        <?php }?>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <input type="text" name="username" id="username" class="form-control input-sm"
                                           placeholder="Username/nickname">
                                </div>
                            </div>
                        </div>
                        <?php  if (isset($errorArray[USR])) { ?>
                        <div class="alert alert-danger">
                            <?php echo $errorArray[USR];?>
                        </div>
                        <?php }?>

                        <div class="form-group">
                            <input type="email" name="email" id="email" class="form-control input-sm"
                                   placeholder="Email Address">
                        </div>
                        <?php  if (isset($errorArray[EML])) { ?>
                            <div class="alert alert-danger">
                                <?php echo $errorArray[EML];?>
                            </div>
                        <?php }?>

                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <input type="password" name="password" id="password" class="form-control input-sm"
                                           placeholder="Password">
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <input type="password" name="password_confirmation" id="password_confirmation"
                                           class="form-control input-sm" placeholder="Confirm Password">
                                </div>
                            </div>
                        </div>
                        <?php  if (isset($errorArray[PSW])) { ?>
                            <div class="alert alert-danger">
                                <?php echo $errorArray[PSW];?>
                            </div>
                        <?php }?>

                        <input type="submit" value="Register" class="btn btn-info btn-block">

                    </form>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 ">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Already a user?
                        <small>Log in here!</small>
                    </h3>
                </div>
                <div class="panel-body">
                    <form role="form" action="<?php echo __SITE_URL; ?>?action=login" method="post">

                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <input type="text" name="username" id="username" class="form-control input-sm"
                                           placeholder="Username/nickname">
                                </div>
                            </div>
                        </div>
                        <?php  if (isset($errorArray[USR_L])) { ?>
                            <div class="alert alert-danger">
                                <?php echo $errorArray[USR_L];?>
                            </div>
                        <?php }?>


                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <input type="password" name="password" id="password" class="form-control input-sm"
                                           placeholder="Password">
                                </div>
                            </div>

                        </div>
                        <?php  if (isset($errorArray[PSW_L])) { ?>
                            <div class="alert alert-danger">
                                <?php echo $errorArray[PSW_L];?>
                            </div>
                        <?php }?>

                        <input type="submit" value="Log in" class="btn btn-success btn-block">

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include(__SITE_PATH . "/views/includes/footer.php"); ?>

