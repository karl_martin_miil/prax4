<?php include(__SITE_PATH . "/views/includes/header.php"); ?>
<style>
    .center-vertically {
        top: 0%;
        position: relative;
        margin-top: 60px;
    }
</style>
<div class="container front-page-container full">
    <div class="row centered-form center-vertically">
        <div class="col-xs-12 col-sm-8 col-md-6  col-md-offset-3 col-sm-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Please sign up for MiniTinder
                        <small>It's free!</small>
                    </h3>
                </div>
                <div class="panel-body">
                    <form role="form" action="<?php echo __SITE_URL; ?>?rt=update&action=userUpdate" method="post">
                        <?php
                        if (isset($errorArray[GER])) { ?>
                            <div class="alert alert-danger">
                                <?php echo $errorArray[GER]; ?>
                            </div>
                        <?php } ?>


                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <input type="text" name="fullname" id="fullname" class="form-control input-sm"
                                           placeholder="Full name">
                                </div>
                            </div>
                        </div>
                        <?php if (isset($errorArray[FN])) { ?>
                            <div class="alert alert-danger">
                                <?php echo $errorArray[FN]; ?>
                            </div>
                        <?php } ?>

                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <label class="checkbox-inline"><input name="gender" type="checkbox"
                                                                          value="m" >Male</label>
                                    <label class="checkbox-inline"><input name="gender" type="checkbox" value="f">Female</label>
                                </div>
                            </div>
                        </div>
                        <?php if (isset($errorArray[GEN])) { ?>
                            <div class="alert alert-danger">
                                <?php echo $errorArray[GEN]; ?>
                            </div>
                        <?php } ?>


                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <input type="number" name="radius" id="radius" class="form-control input-sm"
                                           placeholder="radius">
                                </div>
                            </div>
                        </div>
                        <?php if (isset($errorArray[RAD])) { ?>
                            <div class="alert alert-danger">
                                <?php echo $errorArray[RAD]; ?>
                            </div>
                        <?php } ?>


                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <textarea class="form-control custom-control" rows="3" style="resize:none"
                                              name="description" placeholder="description"></textarea>
                                </div>
                            </div>
                        </div>
                        <?php if (isset($errorArray[DSC])) { ?>
                            <div class="alert alert-danger">
                                <?php echo $errorArray[DSC]; ?>
                            </div>
                        <?php } ?>

                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">

                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <input type="password" name="password" id="password" class="form-control input-sm"
                                           placeholder="Password">
                                </div>
                            </div>
                        </div>
                        <?php if (isset($errorArray[PSW])) { ?>
                            <div class="alert alert-danger">
                                <?php echo $errorArray[PSW]; ?>
                            </div>
                        <?php } ?>

                        <input type="submit" value="Update info and continue" class="btn btn-info btn-block">

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include(__SITE_PATH . "/views/includes/footer.php"); ?>

