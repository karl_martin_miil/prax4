<!DOCTYPE html>
<html class="full" lang="en">
<!-- Make sure the <html> tag is set to the .full CSS class. Change the background image in the full.css file. -->

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Full - Start Bootstrap Template</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo __SITE_URL; ?>views/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo __SITE_URL; ?>views/css/full.css" rel="stylesheet">
    <link href="<?php echo __SITE_URL; ?>views/css/style.css" rel="stylesheet">
    <script src="<?php echo __SITE_URL; ?>views/js/jquery.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>

<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo __SITE_URL; ?>">
                <?php echo isset($_SESSION['username'])? "hi ".$_SESSION['username'] : "MiniTinder";?></a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <?php
                if (isset($s_username)): ?>
                    <!--li>
                        <a href="<?php echo __SITE_URL; ?>?rt=main">Home</a>
                    </li-->
                    <li>
                        <a href="<?php echo __SITE_URL; ?>?rt=findamatch">find a match</a>
                    </li>
                    <li>
                        <a href="<?php echo __SITE_URL; ?>?rt=mymatches">chat with matches</a>
                    </li>
                    <li>
                        <a href="<?php echo __SITE_URL; ?>?rt=update">change user info</a>
                    </li>
                    <li>
                        <a href="<?php echo __SITE_URL; ?>?rt=upload">upload pictures</a>
                    </li>
                    <li>
                        <a href="<?php echo __SITE_URL; ?>?rt=main&action=logout">logout</a>
                    </li>
                <?php else: ?>
                    <li>
                        <a href="<?php echo __SITE_URL; ?>">logIn or register</a>
                    </li>
                <?php endif; ?>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>