<?php include(__SITE_PATH . "/views/includes/header.php"); ?>
<script src="<?php echo __SITE_URL; ?>views/js/jquery.mobile.custom.min.js"></script>
<link href="<?php echo __SITE_URL; ?>views/css/findamatchstyle.css" rel="stylesheet">


    <div class="row">
        <?php
        if (count($persons) > 0) {
            foreach ($persons as $person) {
                ?>
                <div class="buddy" data-id="<?php echo $person['id']; ?>" style="display: block;">
                    <div class="avatar"
                         style="display: block; background-image: url(<?php echo __SITE_URL; ?>uploads/<?php echo $person['url']; ?>)"></div>
                </div>
                <?php
            }
        }
        ?>
    </div>

<script src="<?php echo __SITE_URL; ?>views/js/findamatch.js"></script>

<?php include(__SITE_PATH . "/views/includes/footer.php"); ?>

