<?php include(__SITE_PATH . "/views/includes/header.php"); ?>

<style>

    @import url(//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css);

    body {
        padding: 30px 0px 60px;
    }

    .panel > .list-group .list-group-item:first-child {
        /*border-top: 1px solid rgb(204, 204, 204);*/
    }

    @media (max-width: 767px) {
        .visible-xs {
            display: inline-block !important;
        }

        .block {
            display: block !important;
            width: 100%;
            height: 1px !important;
        }
    }

    #back-to-bootsnipp {
        position: fixed;
        top: 10px;
        right: 10px;
    }

    .c-search > .form-control {
        border-radius: 0px;
        border-width: 0px;
        border-bottom-width: 1px;
        font-size: 1.3em;
        padding: 12px 12px;
        height: 44px;
        outline: none !important;
    }

    .c-search > .form-control:focus {
        outline: 0px !important;
        -webkit-appearance: none;
        box-shadow: none;
    }

    .c-search > .input-group-btn .btn {
        border-radius: 0px;
        border-width: 0px;
        border-left-width: 1px;
        border-bottom-width: 1px;
        height: 44px;
    }

    .c-list {
        padding: 0px;
        min-height: 44px;
    }

    .title {
        display: inline-block;
        font-size: 1.7em;
        font-weight: bold;
        padding: 5px 15px;
    }

    ul.c-controls {
        list-style: none;
        margin: 0px;
        min-height: 44px;
    }

    ul.c-controls li {
        margin-top: 8px;
        float: left;
    }

    ul.c-controls li a {
        font-size: 1.7em;
        padding: 11px 10px 6px;
    }

    ul.c-controls li a i {
        min-width: 24px;
        text-align: center;
    }

    ul.c-controls li a:hover {
        background-color: rgba(51, 51, 51, 0.2);
    }

    .c-toggle {
        font-size: 1.7em;
    }

    .name {
        font-size: 1.7em;
        font-weight: 700;
    }

    .c-info {
        padding: 5px 10px;
        font-size: 1.25em;
    }
</style>

<div class="container">

    <div class="row">
        <div class="col-xs-12 col-sm-offset-3 col-sm-6">
            <div class="panel panel-default">
                <div class="panel-heading c-list">
                    <span class="title">Matches</span>
                    <ul class="pull-right c-controls">
                        <li><a href="#" class="hide-search" data-command="toggle-search" data-toggle="tooltip"
                               data-placement="top" title="Toggle Search"><i class="fa fa-ellipsis-v"></i></a></li>
                    </ul>
                </div>

                <div class="row" style="display: none;">
                    <div class="col-xs-12">
                        <div class="input-group c-search">
                            <input type="text" class="form-control" id="contact-list-search">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="button"><span
                                        class="glyphicon glyphicon-search text-muted"></span></button>
                            </span>
                        </div>
                    </div>
                </div>

                <ul class="list-group" id="contact-list">
                    <?php
                    if (count($persons) > 0):
                    foreach ($persons as $person) {
                        ?>
                        <li class="list-group-item">
                            <div class="col-xs-12 col-sm-3">
                                <img src="<?php echo __SITE_URL."/uploads/".$person['url']; ?>" alt="<?php echo $person['username']; ?>" class="img-responsive img-circle" />
                                <a class="" href="<?php echo __SITE_URL."?rt=chat&subjectId=".$person['userA']; ?>">
                                    go to chat<span class="fa fa-comments text-muted c-info"></span></a>
                            </div>
                            <div class="col-xs-12 col-sm-9">
                                <span class="name"><?php echo $person['username']; ?></span><br/>
                                <span class="name"><?php echo $person['fullname']; ?></span><br/>
                                <div class="span4 collapse-group">
                                    <p class="collapse"><?php echo $person['description']; ?></p>
                                    <p><a class="btn .btn-info" href="#">Read Description </a></p>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </li>
                        <?php
                    }
                    else:
                        echo "no matches found.";
                    endif;
                    ?>

                </ul>
            </div>
        </div>
    </div>


    <!-- JavaScrip Search Plugin -->
    <script src="//rawgithub.com/stidges/jquery-searchable/master/dist/jquery.searchable-1.0.0.min.js"></script>

</div>

<script>
    $(function () {
        /* BOOTSNIPP FULLSCREEN FIX */
        if (window.location == window.parent.location) {
            $('#back-to-bootsnipp').removeClass('hide');
        }


        $('[data-toggle="tooltip"]').tooltip();

        $('#fullscreen').on('click', function (event) {
            event.preventDefault();
            window.parent.location = "http://bootsnipp.com/iframe/4l0k2";
        });

        $('[data-command="toggle-search"]').on('click', function (event) {
            event.preventDefault();
            $(this).toggleClass('hide-search');

            if ($(this).hasClass('hide-search')) {
                $('.c-search').closest('.row').slideUp(100);
            } else {
                $('.c-search').closest('.row').slideDown(100);
            }
        })

        $('#contact-list').searchable({
            searchField: '#contact-list-search',
            selector: 'li',
            childSelector: '.col-xs-12',
            show: function (elem) {
                elem.slideDown(100);
            },
            hide: function (elem) {
                elem.slideUp(100);
            }
        })
    });
    $('.row .btn').on('click', function(e) {
        e.preventDefault();
        var $this = $(this);
        var $collapse = $this.closest('.collapse-group').find('.collapse');
        $collapse.collapse('toggle');
    });
</script>

<?php include(__SITE_PATH . "/views/includes/footer.php"); ?>

