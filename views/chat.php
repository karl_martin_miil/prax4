<?php include(__SITE_PATH . "/views/includes/header.php"); ?>
<style>
    .chat {
        list-style: none;
        margin: 0;
        padding: 0;
    }

    .chat li {
        margin-bottom: 10px;
        padding-bottom: 5px;
        border-bottom: 1px dotted #B3A9A9;
    }

    .chat li.left .chat-body {
        margin-left: 60px;
    }

    .chat li.right .chat-body {
        margin-right: 60px;
    }

    .chat li .chat-body p {
        margin: 0;
        color: #777777;
    }

    .panel .slidedown .glyphicon, .chat .glyphicon {
        margin-right: 5px;
    }

    .panel-body {
        overflow-y: scroll;
        height: 250px;
    }

    ::-webkit-scrollbar-track {
        -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
        background-color: #F5F5F5;
    }

    ::-webkit-scrollbar {
        width: 12px;
        background-color: #F5F5F5;
    }

    ::-webkit-scrollbar-thumb {
        -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, .3);
        background-color: #555;
    }

</style>
<div class="container center-vertically">
    <div class="row">
        <div class="col-md-6 col-md-offset-3 ">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <span class="glyphicon glyphicon-comment"></span>
                </div>
                <div class="panel-body">
                    <ul class="chat">

                    </ul>
                </div>
                <div class="panel-footer">
                    <div class="input-group">
                        <input id="btn-input" type="text" class="form-control input-sm"
                               placeholder="Type your message here..."/>
                        <span class="input-group-btn">
                            <button class="btn btn-warning btn-sm" id="btn-chat">
                                Send
                            </button>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var timeOutVar;
    var username = "<?php echo isset($_SESSION['username'])? $_SESSION['username'] : "";?>";
    var getNewMessagesUrl = location.protocol + '//' + location.host + location.pathname +
        "?rt=chat&action=getNewMessages&subjectId=<?php echo isset($_GET['subjectId'])? $_GET['subjectId'] : "";?>";
    var getOldMessagesUrl = location.protocol + '//' + location.host + location.pathname +
        "?rt=chat&action=getAllMessages&subjectId=<?php echo isset($_GET['subjectId'])? $_GET['subjectId'] : "";?>";


    function getMessages() {
        $.ajax({
            url: getNewMessagesUrl,
            type: "POST",
            data: {getMsg: "John"},
            success: function (data) {
                console.log(data);
                data = $.parseJSON(data);
                $.each(data, function (index, value) {
                    console.log(value);
                    var name = value.text;
                    console.log(value.fec);
                    var time = value.time
                    console.log(value.tex);
                    var text = value.text;
                    var side = "";
                    console.log(username);
                    if (value.sender == username) {
                        side = 'pull-right';
                        name = username;
                    } else {
                        side = 'pull-left';
                        name = value.sender;
                    }

                    $(".chat").append("<li class='right clearfix'><span class='chat-img " + side + "'>" +
                        "<img src='http://placehold.it/50/FA6F57/fff&text=ME' alt='User Avatar' class='img-circle'/>" +
                        "</span><small class=' text-muted'><span class='glyphicon glyphicon-time'></span>" +
                        "" + time + "</small><strong class='" + side + " primary-font'>" + name + " </strong>" +
                        "<br><p>" + text + "</p></li>");


                });
            }
        });
    }

    function getAllPreviousMessages() {
        $.ajax({
            url: getOldMessagesUrl,
            type: "POST",
            data: {getAllMsg: "John"},
            success: function (data) {
                console.log(data);
                data = $.parseJSON(data);
                $.each(data, function (index, value) {
                    console.log(value);
                    var name = value.text;
                    console.log(value.fec);
                    var time = value.time
                    console.log(value.tex);
                    var text = value.text;
                    var side = "";
                    if (value.sender == username) {
                        side = 'pull-right';
                        name = username;
                    } else {
                        side = 'pull-left';
                        name = value.sender;
                    }

                    $(".chat").append("<li class='right clearfix'><span class='chat-img " + side + "'>" +
                        "<img src='http://placehold.it/50/FA6F57/fff&text=ME' alt='User Avatar' class='img-circle'/>" +
                        "</span><small class=' text-muted'><span class='glyphicon glyphicon-time'></span>" +
                        "" + time + "</small><strong class='" + side + " primary-font'>" + name + " </strong>" +
                        "<br><p>" + text + "</p></li>");


                });
            }
        });
    }
    $(document).ready(function () {

        getAllPreviousMessages();
        $("#btn-chat").click(function () {

            var d = new Date();
            var m = d.getMonth() + 1;
            var mes = (m < 10) ? '0' + m : m;
            var dataMsg = $("#btn-input").val();
            var dateTime = d.getFullYear() + '-' + mes + '-' + d.getDate() + ' ' + d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds();
            var nom = '<?php echo isset($_SESSION['username'])? $_SESSION['username'] : "";?>';
            var submitMessageUrl = location.protocol + '//' + location.host + location.pathname +
                "?rt=chat&action=submitMessage&subjectId=<?php echo isset($_GET['subjectId'])? $_GET['subjectId'] : "";?>";

            if ($("#btn-input").val() != "") {

                $.ajax({
                    url: submitMessageUrl,
                    type: "POST",
                    data: {
                        message: dataMsg
                    },
                    dataType: "html",
                    success: function (data) {
                        $(".chat").append(
                            "<li class='right clearfix'><span class='chat-img pull-right'>" +
                            "<img width='50' height='50' src='<?php echo __SITE_URL."uploads/".$picture[0]['url'];?>' alt='User Avatar' class='img-circle'/>" +
                            "</span><small class=' text-muted'><span class='glyphicon glyphicon-time'>" +
                            "</span>" + dateTime + "</small><strong class='pull-right primary-font'>" + nom + " </strong>" +
                            "<br><p>" + dataMsg + "</p></li>");
                    }
                });

                $("#btn-input").val("");
            }
        });
        setInterval(getMessages, 3000);


    });

    $(document).keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            $("#btn-chat").click();
        }
    });


</script>
<?php include(__SITE_PATH . "/views/includes/footer.php"); ?>

