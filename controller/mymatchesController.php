<?php

/**
 * Created by PhpStorm.
 * User: karl
 * Date: 4.12.2015
 * Time: 12:30
 */
class mymatchesController extends baseController
{

    function index()
    {
        $getPersonToMatch = new GetPersonToMatch($this->registry->db);
        $this->registry->template->persons = $getPersonToMatch->getAllMatches($this->getUsernameFromSession());
        $this->registry->template->show('mymatches');
    }
}