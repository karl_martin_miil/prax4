<?php

Class indexController Extends baseController
{

    private $errorArray = array();
    public function index()
    {
        $this->registry->template->errorArray = $this->errorArray;
        $this->registry->template->show('register');
    }

    public function register() {
        $registerUser = new RegisterUser($this->registry->db);
        $validateInput = new ValidateInput($this->registry->db);
        $this->errorArray = $validateInput->validateRegister();
        if (count($this->errorArray) < 1) {
            $userInfo = $registerUser->makeUserAccountInfoArray();
            $registerUser->addUserToDatabase($userInfo);
            header('location: '.__SITE_URL.'?rt=index');
        } else {
            $this->index();
        }
    }

    public function login() {
        $loginUser = new LoginUser($this->registry->db);
        $validateInput = new ValidateInput($this->registry->db);
        $this->errorArray = $validateInput->validateLogin();
        //exit(var_dump($this->errorArray));
        if (count($this->errorArray) < 1) {
            //$ip = $_SERVER['REMOTE_ADDR'];
            $ip = '193.40.244.204';
            $details = json_decode(get_content("http://ipinfo.io/{$ip}/json"));
            //exit(var_dump($details));
            $loginUser->logUserInWithSession();
            $loginUser->updatePositionOfUser($details, $this->getUsernameFromSession());
            header('location: '.__SITE_URL.'?rt=findamatch');
        } else {
            $this->index();
        }
    }


}

?>
