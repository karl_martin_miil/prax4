<?php

/**
 * Created by PhpStorm.
 * User: karl
 * Date: 4.12.2015
 * Time: 12:03
 */
class findamatchController extends baseController
{


    function index()
    {
        $getPersonToMatch = new GetPersonToMatch($this->registry->db);
        $userDbInfoParser = new UserDbInfoParser($this->registry->db);
        $personsWithPicture = $getPersonToMatch->getPersons($this->getUsernameFromSession(), $userDbInfoParser);
        $this->registry->template->persons = $personsWithPicture;
        $this->registry->template->show('findamatch');
    }

    function like() {
        $getPersonToMatch = new GetPersonToMatch($this->registry->db);
        if (isset($_GET['likeId'])) {
            $getPersonToMatch->addPersonLike($this->getUsernameFromSession(), $_GET['likeId']);
        }
    }
}