<?php

/**
 * Created by PhpStorm.
 * User: karl
 * Date: 3.12.2015
 * Time: 12:10
 */
class uploadController extends baseController
{






    function index()
    {
        $helper = new Helper($this->registry->db);
        $pictureArray = $helper->getAllPicturesOfPerson($this->getUsernameFromSession());
        $this->registry->template->pictures = $pictureArray;
        $this->registry->template->show('upload');
    }

    function upload() {
        $uniId = uniqid();
        $fileUploader = new UploadFile($this->registry->db);
        $helper = new Helper($this->registry->db);
        $personId = $helper->getUserId($this->getUsernameFromSession());
        if ($fileUploader->upload_my_file($uniId)) {
            if ($fileUploader->addPictureUrlToDb($uniId, $personId)) {
                $this->index();
            }
        }
    }
}