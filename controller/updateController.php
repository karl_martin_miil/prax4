<?php

/**
 * Created by PhpStorm.
 * User: karl
 * Date: 3.12.2015
 * Time: 10:19
 */
class updateController extends baseController {

    private $errorArray;

    public function index() {
        $this->registry->template->errorArray = $this->errorArray;
        $this->registry->template->show('update');
    }

    public function userUpdate() {
        $updateUser = new UpdateUser($this->registry->db);
        $validateInput = new ValidateInput($this->registry->db);
        $this->errorArray = $validateInput->validateUpdate();
        if (count($this->errorArray) < 1) {
            $userInfo = $updateUser->makeUserAccountInfoArray();
            if ($updateUser->updateUserInfoInDb($userInfo)){
                header('location: '.__SITE_URL.'?rt=main');
            }else{
                exit("it broke in user update");
            }
        } else {
            $this->index();
        }
    }


}