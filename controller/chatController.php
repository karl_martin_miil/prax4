<?php

/**
 * Created by PhpStorm.
 * User: karl
 * Date: 4.12.2015
 * Time: 20:53
 */
class chatController extends baseController
{
    function index()
    {
        $chatRoom = new ChatRoom($this->registry->db);
        $helper = new Helper($this->registry->db);
        $this->registry->template->picture = $helper->getFirstPictureOfPerson($this->getUsernameFromSession());
        if (isset($_GET['subjectId'])) {
            $chatRoom->getOldChatMessages($this->getUsernameFromSession(), $_GET['subjectId']);
        }
        $this->registry->template->show('chat');

    }

    function submitMessage()
    {
        if (isset($_GET['subjectId']) && isset($_POST['message'])) {
            $chatRoom = new ChatRoom($this->registry->db);
            $chatRoom->submitMessage($this->getUsernameFromSession(), $_GET['subjectId'], $_POST['message']);
        }
    }
    function getNewMessages() {
        if (isset($_GET['subjectId']) && isset($_POST['getMsg'])) {
            $chatRoom = new ChatRoom($this->registry->db);
            //exit(date("Y-m-d H:i:s"));
            $answer = $chatRoom->getNewChatMessages($this->getUsernameFromSession(), $_GET['subjectId'], date("Y-m-d H:i:s", time()-4));
            echo(json_encode($answer));
        }
    }
    function getAllMessages() {
        if (isset($_GET['subjectId']) && isset($_POST['getAllMsg'])) {
            $chatRoom = new ChatRoom($this->registry->db);
            $answer = (array)$chatRoom->getOldChatMessages($this->getUsernameFromSession(), $_GET['subjectId']);
            echo(json_encode($answer));
        }
    }
}